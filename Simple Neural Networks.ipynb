{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# What are neural networks?\n",
    "\n",
    "In this worksheet, we are going to see that neural networks are machines that can calculate two things: function values and their gradients. The functions on which neural networks work well are functions constructed from many simple functions, through aggregation operators like composition and summation. \n",
    "\n",
    "To observe this, we will reconstruct some functions using pytorch. The functions are deliberately simple, so we can also have the explicit formulas for their gradients. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "import numpy as np\n",
    "from collections import OrderedDict \n",
    "#import torchviz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# A few helper functions.\n",
    "\n",
    "#Set the value for the parameters:\n",
    "def setvalue(x,v):\n",
    "     torch.nn.init.constant_(x,v)\n",
    "        \n",
    "# Get the output (z):\n",
    "def getvalue(x):\n",
    "    return x.detach().numpy()[0,0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A simple Neural Network\n",
    "\n",
    "Let us start with a very small network: 2 input variables, one output variable, and one hidden layer of 4 nodes. The first layer is linear, and the second layer is affine.\n",
    "\n",
    "Denote by $x$ the input variable, $y$ the \"hidden\" variable, and $z$ the output. Then we have $y = wx$ and $z = vy+b$, meaning that $z = vwx+b$.\n",
    "So, $z$ is a linear function on $x$, by composition of two linear functions.."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# N is batch size; D_in is input dimension;\n",
    "# H is hidden dimension; D_out is output dimension.\n",
    "N, D_in, H, D_out = 1, 1, 1, 1\n",
    "\n",
    "# Create the model (sequential model):\n",
    "model = torch.nn.Sequential() # putting the layers one after anothers.\n",
    "\n",
    "# Create and add the first layer, \n",
    "# A linear function wx:\n",
    "wn = torch.nn.Linear(D_in,H,bias=False)\n",
    "model.add_module('w',wn)\n",
    "\n",
    "# Create and add the first layer, \n",
    "# An affine function with weight vy+b:\n",
    "vn = torch.nn.Linear(H,D_out,bias=True)\n",
    "model.add_module('v',vn)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Initialise the input to the appropriate dimension:\n",
    "xn = torch.ones(N,D_in)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluating the function\n",
    "\n",
    "Using the network, we can now evaluate the function. Let us compare the function, defined explicitly, with the output of the network:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the function explicitly:\n",
    "def z(v,w,b,x):\n",
    "    \"\"\"\n",
    "    Define the value of z through the explicit formula\n",
    "    \"\"\"\n",
    "\n",
    "    return v*w*x+b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate the function through the neural network:\n",
    "def zn(v,w,b,x):\n",
    "    \"\"\"\n",
    "    Obtain the value of z through the neural network\n",
    "    \"\"\"\n",
    "    # First we set the values of all the parameters inside the network:\n",
    "    p = dict(model.named_parameters())\n",
    "    setvalue(p['w.weight'],w)\n",
    "    setvalue(p['v.weight'],v)\n",
    "    setvalue(p['v.bias'],b)\n",
    "    setvalue(xn,x)\n",
    "    \n",
    "    out = model(xn)\n",
    "\n",
    "    # Then we run the network and get the output value.\n",
    "    return getvalue(out)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compare the values:\n",
    "print(\"Direct calculation:\",z(1.2,-1,1,1))\n",
    "print(\"Through the neural network:\",zn(1.2,-1,1,1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now we compare the values for the functions zn and z over a grid:\n",
    "N=5\n",
    "compared_values = np.array([\n",
    "    zn(v,w,b,x) == z(v,w,b,x) # Check that the functions z and zn return the same value\n",
    "    for v in range(-N,N) for w in range(-N,N) for b in range(-N,N) for x in range(-N,N) # For a whole bunch of input\n",
    "    ])\n",
    "\n",
    "print(\"The values are equal everywhere: \",np.all(compared_values))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluating the derivatives\n",
    "\n",
    "Using the network we can also evaluate the gradients. The gradient of our function is:\n",
    "$$\\frac{\\partial z}{\\partial w} = vx,\\;\\frac{\\partial z}{\\partial v} = wx,\\;\\frac{\\partial z}{\\partial b} = 1$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dz(v,w,b,x):\n",
    "    \"\"\"\n",
    "    Define the gradients v,w and b through the explicit formula\n",
    "    \"\"\"\n",
    "    return np.array([v*x,w*x,1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dzn(v,w,b,x):\n",
    "    \"\"\"\n",
    "    Obtain the gradient of z through the neural network\n",
    "    \"\"\"\n",
    "    # We need to reset the gradient to zero, because otherwise gradient calculations are\n",
    "    # adding to the previous values.\n",
    "    model.zero_grad()\n",
    "    # Then we set the values of all the parameters inside the network:\n",
    "    p = dict(model.named_parameters()) # Reset the model\n",
    "    ww = p['w.weight']\n",
    "    vw = p['v.weight']\n",
    "    vb = p['v.bias']\n",
    "    setvalue(ww,w)\n",
    "    setvalue(vb,b)\n",
    "    setvalue(vw,v)\n",
    "    setvalue(xn,x)\n",
    "    # Run the model\n",
    "    zn = model(xn)\n",
    "    # Compute gradient (by running the model backwards)\n",
    "    zn.backward()\n",
    "    # Get the gradients for all the parameters inside the network.\n",
    "    return np.array([ww.grad.numpy()[0,0],vw.grad.numpy()[0,0],vb.grad.numpy()[0]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dz(1,2,3,4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dzn(1,2,3,4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compare the values over a grid:\n",
    "N = 5\n",
    "compared_gradients = np.array([\n",
    "    dzn(v,w,b,x) == dz(v,w,b,x)  # Check that the functions calculating the gradients are equal\n",
    "    for v in range(-N,N) for w in range(-N,N) for b in range(-N,N) for x in range(-N,N) # for a whole bunch of values on a grid.\n",
    "    ])\n",
    "\n",
    "print(\"The gradients are equal everywhere: \",np.all(compared_values))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A more advanced example\n",
    "\n",
    "Let us now look at a more advanced example: $$\\begin{aligned}\n",
    "y_j(x) &= \\sum_{i=1}^{n} w_{ij} x_i = wx\\\\\n",
    "z_k(x) &= \\sum_{j=1}^{m} v_{jk} y_j + b_k = zy+b\\\\\n",
    "\\end{aligned}$$\n",
    "\n",
    "The objective function (or *loss function*) is then $$f(x) = \\sum_{k=1}^{p} \\big(z_k(y(x))\\big)^2 = \\|z(y(x))\\|^2$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# n is batch size; D_in is input dimension;\n",
    "# H is hidden dimension; D_out is output dimension.\n",
    "N, n, m, p = 1, 10, 13, 5\n",
    "\n",
    "# Create random Tensors to hold inputs and outputs\n",
    "xn = torch.ones(N, n)  # Set x=(1,1,...,1)\n",
    "zn = torch.zeros(N, p) # Set z = (0,0,...,0)\n",
    "\n",
    "model = torch.nn.Sequential()\n",
    "\n",
    "wn = torch.nn.Linear(n, m,bias=False) # w is a linear function. \"bias=false\" means that there is no constant term.\n",
    "model.add_module('w',wn)\n",
    "\n",
    "vn = torch.nn.Linear(m,p,bias=True) # v is also a linear function, this time with a constant term.\n",
    "model.add_module('v',vn)\n",
    "\n",
    "loss = torch.nn.MSELoss(reduction='sum')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "torch.nn.init.ones_(wn.weight)\n",
    "torch.nn.init.constant_(vn.weight,3)\n",
    "torch.nn.init.ones_(vn.bias)\n",
    "\n",
    "#torchviz.make_dot(model(xn),params=dict(model.named_parameters()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def z(v,w,b,x):\n",
    "    \"\"\"\n",
    "    Define the value of z through the explicit formula\n",
    "    \"\"\"\n",
    "    return np.sum((v.dot(w.dot(x))+b)**2)\n",
    "\n",
    "def zn(v,w,b,x):\n",
    "    \"\"\"\n",
    "    Obtain the value of z through the neural network\n",
    "    \"\"\"\n",
    "    p = dict(model.named_parameters())\n",
    "    torch.nn.init.constant_(p['w.weight'],w)\n",
    "    torch.nn.init.constant_(p['v.bias'],b)\n",
    "    torch.nn.init.constant_(p['v.weight'],v)\n",
    "    torch.nn.init.constant_(xn,x)\n",
    "    return model(xn).detach().numpy()[0,0]\n",
    "\n",
    "#Generate a random input.\n",
    "zer = torch.zeros(p)\n",
    "xn = torch.rand((n))\n",
    "wn.weight.data = torch.rand((13,10))\n",
    "vn.weight.data = torch.rand((5,13))\n",
    "vn.bias.data = torch.rand((5))\n",
    "\n",
    "x = xn.numpy()\n",
    "w = wn.weight.detach().numpy()\n",
    "v = vn.weight.detach().numpy()\n",
    "b = vn.bias.detach().numpy()\n",
    "\n",
    "#wn.weight = torch.rand(m)\n",
    "print(z(v,w,b,x), loss(model(xn),zer))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
