---
title: Optimisation algorithms
bibliography: algorithms.bib
biblatex: true
amsthms: ["rule","theorem","example","lemma","corollary","proposition","definition","note"]
cref: true
figPrefix: [Figure,Figures]
---

# Introduction

## Motivation: what is an artificial neural network?

We start from considering the optimisation problem: 

## Optimisation algorithms

Let us consider the optimisation problem: 
$$\text{minimise} f(\vx), \text{ subject to } \vx\in \mathcal{X}$$

Definition

: * $f$ is the *objective function*;
  * $\mathcal{X}$ is the *feasible set*.


* Sometimes $\mathcal{X}$, is the solution set of a number of equations and inequalities: $\mathcal{X} = \{\vx: g_i(\vx)\leq 0, h_j(\vx)=0: i=1,\ldots,n, j=1,\ldots,m \}$
* When $\mathcal{X}=\R^n$, we call this problem *unconstrained*.
 

An optimisation algorithm is a procedure taking, as an input, an optimisation problem, and produces a sequence of iterations to (hopefully) produce solutions (or near-solutions) to the optimisation problem. At each iteration, the procedure evaluates the objective function at a given point (the *iterate*) to gain information about it and construct the next iterate. Here is a more formal definition:

Definition

:  Denote by $\mathbf{\vx^m} = [x_1,\ldots,x_m]$ an ordered list of $m$ iterates, and $\mathbf{y^m} = [f(x_1),\ldots,f(x_m)]$ their function values.

   The iterate $x_{m+1}$ produced by an algorithm $a$ depends of the pair $(\vx,\mathbf{y})$: $x_{m+1} = a(\mathbf{(\vx,\mathbf{y})})$.
   Therefore an optimisation algorithm $a$ is a mapping from the set of iterates $(\vx^m,\mathbf{y}^m)$ to $\mathcal{X}\setminus \vx^m$.


If we know *nothing* of the function $f$, we cannot do much. Any algorithm is equally likely to succeed in producing any sequence of function values. When $f: \mathcal{X}\to \mathcal{Y}$ and both $\mathcal{X}$ and $\mathcal{Y}$ are finite sets, this is embodied by the *no free lunch theorem*:

Theorem (No free lunch theorem [@nfl])

: For any pair of algorithms $a_1$ and $a_2$, 
  $$ \sum_{f} P(\mathbf{y}^m\mid f,a_1) = \sum_{f} P(\mathbf{y}^m\mid f,a_2) = \left|\mathcal{Y}\right|^{\left|\mathcal{X}\right|-m} $$

  In particular, with no prior assumption on $f$, $$P(\mathbf{y}^m\mid a_1) = P(\mathbf{y}^m\mid a_2) = \frac{1}{\left|\mathcal{Y}\right|^{m}}$$

Proof

: The idea behind the proof is the following: after $m$ iterations the algorithm has produced a sequence of iterates $(\vx^m,\mathbf{y}^m)$, which it uses to generate a new iterate $x_{m+1}$. For any given $y$, there are functions such that $f(x_{m+1}) = y$. In fact the number of such functions doesn't depend on the algorithm, nor on $y$, nor on $\mathcal{\vx}^m$, but only on the sizes of the sets and the number of past iterations $|\mathcal{Y}|$, $|\mathcal{X}|$ and $m$. So, if we assume nothing of the function, any algorithm has the same probability of producing a new iterate with function value $y$.

  To formalise this proof, we need to rely on probability calculations, in particular conditional probabilities. The proof is a bit tedious and long, because in order to carry out our idea, we have to break down the probability calculations over all possible sequences of iterates, and over all possible functions.

  We prove this by induction, starting at $m=1$. 
  There are $\left|\mathcal{Y}\right|^{\left|\mathcal{X}\right|}$ functions from $\mathcal{X}$ to $\mathcal{Y}$ altogether, $\left|\mathcal{Y}\right|^{\left|\mathcal{X}\right|-1}$ of which have $f(d_1^y)=y$.

  So, 
  $$\sum_{f} P(\mathbf{y}^1\mid f,a) = \sum_{f: f(x_1) = y_1)} \underbrace{P(\mathbf{y}^1\mid f,a)}_{=1} + \sum_{f: f(x_1) \neq y_1)} \underbrace{P(\mathbf{y}\mid f,a)}_{=0} =\left|\mathcal{Y}\right|^{\left|\mathcal{X}\right|-1}$$

  Assume that the statement is true for $m$ and we will prove it for $m+1$.
  $$\begin{aligned}
  P(\mathbf{y}^{m+1}\mid f,a) &= P(y_{m+1}\mid \mathbf{y}^m,f,a)\cdot P(\mathbf{y}^{m}\mid f,a)\\
  &= \sum_{x_{m+1}\in \mathcal{X}} P(y_{m+1}\mid f,x_{m+1})\cdot P(x_{m+1}\mid \mathbf{y}^m,f,a)\cdot P(\mathbf{y}^{m}\mid f,a)\\
  &= \sum_{x_{m+1}\in \mathcal{X}} \sum_\mathbf{\mathbf{\vx^m}} P(y_{m+1}\mid f,x_{m+1})\cdot P(x_{m+1}\mid (\vx^m,\mathbf{y}^m),f,a)\cdot \underbrace{P(\vx^m|\mathbf{y}^m,f,a)\cdot P(\mathbf{y}^{m}\mid f,a)}_{P\big((\vx^m,\mathbf{y}^m)|f,a\big)}\\
  &= \sum_\mathbf{\mathbf{\vx^m}} P\big(y_{m+1} =  f(a(\vx^m,\mathbf{y}^m))\big)\cdot P\big((\vx^m,\mathbf{y}^m)|f,a\big)\\
  \end{aligned} $$

  Now we can take the sum over all functions.
  $$ \sum_{f}P(\mathbf{y}^{m+1}\mid f,a) 
  = \sum_{f}\sum_\mathbf{\mathbf{\vx^m}}  P\big(y_{m+1} =  f(a(\vx^m,\mathbf{y}^m))\big)\cdot P\big((\vx^m,\mathbf{y}^m)|f,a\big)\\  $$

  Note that $P\big(y_{m+1} =  f(a(\vx^m,\mathbf{y}^m))\big)$ depends only on the values of $f$ on the set $\vx^m$. So, to calculate the sum above, we can notice that functions from $\mathcal{X}\to \mathcal{Y}$ are the Cartesian products of two sets of functions: those defined on $\vx^m$ and those defined on $\mathcal{X}\setminus \vx^m$. We can use that to simplify the sum:

  $$\begin{aligned}
  \sum_{f}P(\mathbf{y}^{m+1}\mid f,a) 
  &= \sum_\mathbf{\mathbf{\vx^m}} \sum_{f} P\big(y_{m+1} =  f(a(\vx^m,\mathbf{y}^m))\big)\cdot P\big((\vx^m,\mathbf{y}^m)|f,a\big)\\ 
  &= \sum_\mathbf{\mathbf{\vx^m}} \sum_{f|_{\vx^m}}\sum_{f|_{\mathcal{X}\setminus \vx^m}} P\big(y_{m+1} =  f(a(\vx^m,\mathbf{y}^m))\big)\cdot P\big((\vx^m,\mathbf{y}^m)|f,a\big)\\
  &= \sum_\mathbf{\mathbf{\vx^m}} \sum_{f|_{\vx^m}} P\big((\vx^m,\mathbf{y}^m)|f,a\big) \underbrace{\sum_{f|_{\mathcal{X}\setminus \vx^m}} P\big(y_{m+1} =  f(a(\vx^m,\mathbf{y}^m))\big)}_{\left|\mathcal{Y}\right|^{\left|\mathcal{X-m-1}\right|} } \\
  &= \left|\mathcal{Y}\right|^{\left|\mathcal{X-m-1}\right|} \sum_\mathbf{\mathbf{\vx^m}} \sum_{f|_{\vx^m}} P\big((\vx^m,\mathbf{y}^m)|f,a\big) \\
  &= \left|\mathcal{Y}\right|^{\left|\mathcal{X-m-1}\right|} \sum_\mathbf{\mathbf{\vx^m}} \sum_{f|_{\vx^m}} P\big((\vx^m,\mathbf{y}^m)|f,a\big) \cdot \sum_{f|_{\mathcal{X}\setminus \vx^m}} 1 \cdot \frac{1}{\sum_{f|_{\mathcal{X}\setminus \vx^m}} 1}\\
  &= \frac{\left|\mathcal{Y}\right|^{\left|\mathcal{X-m-1}\right|}}{\left|\mathcal{Y}\right|^{\left|\mathcal{X-m}\right|}} \sum_\mathbf{\mathbf{\vx^m}} \sum_{f|_{\vx^m}} \sum_{f|_{\mathcal{X}\setminus \vx^m}} P\big((\vx^m,\mathbf{y}^m)|f,a\big)\\
  &= \frac{1}{\left|\mathcal{Y}\right|} \sum_\mathbf{\mathbf{\vx^m}} \sum_{f} P\big((\vx^m,\mathbf{y}^m)|f,a\big)\\
  &= \frac{1}{\left|\mathcal{Y}\right|} \sum_{f} \sum_\mathbf{\mathbf{\vx^m}} P\big((\vx^m,\mathbf{y}^m)|f,a\big)\\
  &= \frac{1}{\left|\mathcal{Y}\right|} \sum_{f} P\big(\mathbf{y}^m|f,a\big)\\
  &= \frac{1}{\left|\mathcal{Y}\right|} \left|\mathcal{Y}\right|^{\left|\mathcal{X}\right|-m}\\
  &= \left|\mathcal{Y}\right|^{\left|\mathcal{X}\right|-m-1}
  \end{aligned}$$

  In particular, if we make no assumption on $f$, that is, if $P(f) = \frac{1}{\left|Y\right|^{\left|X\right|}}$, then:
  $$\begin{aligned}
  P(\mathbf{y}^m\mid a) &= \sum_{f} P(\mathbf{y}^m\mid f) P(f)
                      &&= \frac{1}{\left|\mathcal{Y}\right|^{\left|X\right|}}\sum_{f} P(\mathbf{y}^m\mid f)\\
                      &= \frac{1}{\left|\mathcal{Y}\right|^{\left|X\right|}}\left|\mathcal{\mathcal{Y}}\right|^{\left|\mathcal{X}\right|-m-1}
                      &&= \frac{1}{\left|\mathcal{Y}\right|^{\left|X\right|}}\left|\mathcal{\mathcal{Y}}\right|^{\left|\mathcal{X}\right|-m}\\
                      &= \frac{1}{\left|\mathcal{Y}\right|^m}&&&\qedhere
  \end{aligned}$$

The \emph{No Free Lunch} theorem only applies to optimisation problems over finite sets, and it is difficult to generalise it over more interesting problems (such as on $\R^n$). On the other hand, it is quite powerful. What it says is that, if we do not know anything about our objective function, no algorithm can be expected to be better than just trying solutions randomly.

It *also* says that if you specialise your algorithm for specific types of functions, it is likely to perform significantly better than a random algorithm (and, mechanically, to perform *worse* on functions outside of its scope). **The more you specialise your algorithm the likelier it is to perform well on the problems you specialise for.**

Note: In practice, we will assume certain properties on our functions.  You have already seen specialised methods for the case when the objective is *linear* and the set $\mathcal{X}$ is a *polytope*, and when the function $f$ and the set $\mathcal{X}$ are *convex*.
We will only consider *continuous* functions, sometimes *differentiable*, or even *twice differentiable*.

# Unconstrained Optimisation

We start with the optimisation problem: 

$$\text{minimise } f(\vx), \text{ subject to } \vx\in \mathcal{X}$$
where $\mathcal{X}$ is typically $\R^n$.

Many optimisation algorithms belong to the family of *iterative descent* methods. The sequence of iterates they produce satisfies the following property:
$$f(\vx^{k+1}) < f(\vx^k).$$

Note
: Not all iterative descent methods will converge to a "good" solution. Consider for example
$f(\vx) = \vx^2$, whose unique minimum occurs at $\vx=0$. Let

  $$\vx^k = \begin{cases}
1+\frac{1}{k+1} & \text{if $k$ is even}\\
-1-\frac{1}{k+1} & \text{if $k$ is odd}\\
\end{cases} $$

  Although we have $f(\vx^{k+1}) < f(\vx^{k})$, $$\lim_{k\to \infty} f(\vx^k) = 1 \neq 0.$$

  \input{non_convergence.tikz}


In most iterative descent algorithms, the iterates are generated following a *descent direction*: $\vx^{k+1} = \vx^{k}  + \alpha_k \vd^k$, where $\vd^k$ satisfies the property that $t\mapsto f(\vx^k+ t\vd^k)$ is a decreasing function over an interval $[0,K]$ for some $K>0$. Generating the descent direction $\vd^k$ and the step size $\alpha_k$ is where these algorithms differ.

## Optimality Conditions

We briefly recall some optimality conditions.

Theorem (First-order optimality conditions):  Suppose that $f$ is differentiable at $\vx^*$. Then
if $f$ attains its minimum at $\vx^*$, then $\nabla f(\vx^*) = 0$.

Theorem (Second-order optimality conditions):
: Suppose $f$ is twice differentiable at $\vx^*$.

  1. If $\vx^*$ is a local minimiser of $f$ then $\nabla f(\vx^*) = 0$ and  $\nabla^2 f(\vx^*)\succeq 0$
  2. If $\nabla f(\vx^*) = 0$ and $\nabla^2 f(\vx^*)\succ 0$, then $\vx^*$ is a local minimiser of $f$.


## Differentiable functions

In this section we assume that the function $f$ is $C^1$, that is it is differentiable, with a continuous derivative.

When that is the case, our strategy will be to approximate the function locally. Using Taylor expansions, we can approximate the function using a linear or a quadratic function, for which we have well-established strategies.

### Descent directions

Proposition
: Suppose $f$ is $C^1$. 

  1. If $\langle \vd^k,\nabla f(\vx^k)\rangle < 0$, then $\vd^k$ provides a direction of descent.
  2. If $\vd^k$ provides a direction of descent, then $\langle \vd^k,\nabla f(\vx^k)\rangle \leq 0$.

Proof
: The main idea of the proof is to "slice" the function $f$ along the direction $\vd$, to get a function of one variable. Then, we can talk about derivatives instead of gradients (the *directional derivative*), and the derivative at a point corresponds to the slope of the function at this point. Knowing that the directional derivative can be obtained from the gradient, we then can obtain the result.

  Applying Taylor's expansion on the continuously differentiable function $f$ around $\vx$ gives us that $$f(\vx+\alpha \vd) - f(\vx) = \alpha \langle \nabla f(\vx), \vd\rangle + o(\alpha).$$

  If $\langle \nabla f(\vx), \vd\rangle < 0$, there is a value $\alpha_0>0$ such that $f(\vx+\alpha \vd) - f(\vx) < 0$ for $\alpha\in (0,\alpha_0)$, which is the definition of a descent direction. 

  Going the other way, if $\vd$ is a direction of descent, then $f(\vx+\alpha \vd) - f(\vx) < 0$ for $\alpha$ small enough. Taking the limit (which exists, since $f$ is differentiable): $$\lim_{\alpha \to 0^+} \frac{f(\vx+\alpha \vd) - f(\vx)}{\alpha} \leq 0.$$

Note: In practice, we want to make sure that the descent direction is steep enough: $\langle \vd^k,\nabla f(\vx^k)\rangle < \|\nabla f(\vx^k)\|$


#### Sufficient descent

Theorem: If $$\|\nabla f(\vx^{k+1})\| < \delta \|\nabla f(\vx^{k})\|$$ for $0 < \delta <1$, then at any accumulation point $\vx^*$ of the sequence $\vx^k$, $\nabla f(\vx^*) = 0$.

Proof: For any $k$, $$\|\nabla f(\vx^k)\| < \delta^k \|\nabla f(\vx^0)\|$$ and so $$\lim_{k\to \infty} \|\nabla f(\vx^k)\| = 0.$$ By continuity of $\|\nabla f\|$, this implies that $\nabla f(\vx^*) = 0$.

Example: In the example above, for any $\delta$ there exists $k_0$ such that for any $k>k_0$, $$\frac{\|\nabla f(\vx^{k+1})\|}{\|\nabla f(\vx^k)\|} > \delta.$$

### Gradient descent methods

We start from the most elementary method, the steepest descent method. We assume that the function $f$ is differentiable.

\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwInOut{Input}{Input}
\SetKwInOut{Initialise}{Initialise}
\Input{a starting point \(x_0\)}
\Initialise{Set $k=0$.}
\While{$\nabla f(x_k)\neq 0$}{
Set $d_k=-\nabla f(x_k)$\;
Set $x_{k+1} = x_k + \alpha_k d_k$\;
Set $k = k+1$\;
}
\caption{Steepest Descent Algorithm}
\end{algorithm}

There are variations between algorithms, depending on how $\alpha_k$ is chosen. Later we will see techniques to select $\alpha_k$. For now, what is important is that $\alpha_k$ provides a *sufficient descent*.

Theorem
: The direction $d_k = -\nabla f(\vx_k)$ provides the direction of *steepest descent*:

  $$\Big\langle \frac{d_k}{\|d_k\|}, \nabla f(\vx^k)\Big\rangle \leq \langle \vd, \nabla f(\vx^k)\rangle, \forall \vd\in B(0,1)$$

Proof
: Take any direction $\vd$ such that $\|\vd\|\leq 1$.

  We have already seen that the "slope" is given by the directional derivative, $\langle \vd,\nabla f(\vx^k),\rangle$. By Cauchy-Schwartz, $\langle \vd,\nabla f(\vx^k),\rangle \geq -\|\vd\|\|\nabla f(\vx^k)\|$. So, since $\|\vd\|\leq 1$, $\langle \vd,\nabla f(\vx^k),\rangle \geq -\|\nabla f(\vx^k)\| = \langle -\frac{\nabla f(\vx^k)}{\|\nabla f(\vx^k)\|},\nabla f(\vx^k)\rangle$.


### Line Searches

There are several ways to pick $\alpha_k$ in the gradient descent algorithms. In principle, we could perform an *exact* line search:
$$\alpha_k = \argmin_{\alpha>0} f(\vx+\alpha\vd^k)$$ 

The problem here is that the exact line search is hard and possibly costly. It is often more efficient to perform an *inexact* line search. As we have seen before, what we need to have a *sufficient descent*. The following rules are commonly used:

Rule (Armijo's Rule): Let $c\in (0,1)$.  The step size $\alpha$ satisfies Armijo's rule if
$$ f(\vx) - f(\vx + \alpha \vd) \geq -c\alpha f'(\vx,\vd) = -c\alpha \langle \nabla f(\vx),\vd\rangle $$

Rule (Goldstein's Rule):
For $c\in (0,\tfrac{1}{2})$, the step size $\alpha$ satisfies Armijo's rule if
$$\sigma \leq \frac{f(\vx + \alpha \vd) - f(\vx)}{\alpha f'(\vx,\vd)} \leq 1-\sigma$$

Note: Armijo's rule is a simplified version of Goldstein's rule. It is generally preferred in practice.

To find a step size that satistfies Armijo's rule, we can apply a *backtracking* algorithm:

\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwInOut{Input}{Input}
\SetKwInOut{Initialise}{Initialise}
\Input{\(\gamma\in (0,1)\)}
\Initialise{a starting step \(\alpha_0\)}
Set $k=0$.
\While{\(\alpha_k\) doesn't satisfy Armijo's rule}{
Set $k=k+1$\;
Set $\alpha_k = \gamma^k\alpha_0 = \gamma \alpha_{k-1}$.
}
\Return{$\alpha_k$}
\caption{Backtracking Algorithm}
\end{algorithm}

A more general rule is to have a *diminishing step size*:

Rule (diminishing step size):  $$\lim_{k\to\infty} \alpha_k = 0 \; \sum_{k=0}^{\infty} \alpha_k = \infty$$

### Problems with the steepest gradient method

The steepest gradient method tends to have a zigzag-like behaviour. In order to avoid this behaviour, we should implement "correcting" strategies, that alter the direction, that is, from a matrix $D_k$ we use $d_k = D_k \nabla f(\vx^k)$. 

### Newton method

Where the steepest descent method relies on the *first-order* approximation of the function (its gradient), if the function is twice differentiable, we can use the *second-order* approximation instead. This is what Newton method does.

First suppose that the function $f=\tfrac{1}{2}\vx^TQ\vx + b\vx + c$ is actually quadratic. In that case, we can find its minimiser by solving the equation $Q\vx=-b$, and if $Q$ is positive definite, we know that $\vx^* = -Q^{-1}b$.

When the function is *not* quadratic, but twice differentiable, we can construct a local quadratic approximation using the Taylor expansion:
$f(\vx+\vd) - f(\vx)\approx \nabla f(\vx)^T \vd + \vd^T\tfrac{1}{2}(\nabla^2f(\vx))\vd$.

So, (if we are lucky and $\nabla^2f(\vx)$ is positive definite) we can find a "good" direction using the formula: $\vd^{k} = -(\nabla^2f(\vx)^{-1}\nabla f(\vx^k)$.

\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwInOut{Input}{Input}
\SetKwInOut{Initialise}{Initialise}
\Input{a starting point \(x_0\)}
\Initialise{Set $k=0$.}
\While{$\nabla f(x_k)\neq 0$}{
Set $d_k=-(\nabla^2f(x_k)^{-1}\nabla f(x_k)$\;
Set $x_{k+1} = x_k + \alpha_k d_k$\;
Set $k = k+1$\;
}
\caption{Newton Algorithm}
\end{algorithm}

It is not easy to use Newton's algorithm, because the Hessian is not always positive definite, and even when it is, it is quite costly to calculate. However, some algorithm are based on the same idea, by constructing positive definite matrices that play the role of the Hessian.

### Quasi-Newton methods

Instead of calculating the Hessian, we can use past gradient information. Let us first consider the situation when the objective function $f = \tfrac{1}{2} \vx^TQ\vx +b$ is quadratic. The gradient $\nabla f(\vx) = Qx+b$ is linear, and the Hessian $\nabla^2 f(\vx) = Q$ is constant.

$$ \nabla f(\vx+\vd) - \nabla f(\vx) = Q(\vx+\vd) +b - (Qx+b) = Qd = \nabla^2 f(\vx) \vd.$$

The idea behind Quasi-Newton methods is to use the formula:
$$ \nabla f(\vx+\vd) - \nabla f(\vx) \approx \nabla^2 f(\vx) \vd.$$

Suppose that we have $n$ linearly independent directions $d_1,\ldots,d_n$ for which we have an approximation of $\nabla^2 f(\vx) \vd$ (for example, after performing $n$ steps of a descent algorithm). Then, using the full rank matrix $B=[d_1,\ldots,d_n]$ and the corresponding matrix $P=[p_1,\ldots,p_n]$, we have $P = \nabla^2 f(\vx) B$. Solving this system gives us $PB^{-1} = \nabla^2 f(\vx)$.

#### BFGS method

Quasi-Newton methods give us a way to apply a Newton-like method without calculating the Hessian. However, it requires us to invert a matrix at every iteration, which isn't much better... or does it?

At iteration $k$, we already have an approximation of the Hessian. Rather than re-compute the approximation $B_k$ at every iteration, we can update it using our new knowledge of $\nabla f(\vx^{k+1})$:

- We perform a "small" change in $B_k$, which means that we add two rank-one matrices ($uu^T$ and $vv^T$);
- We want to maintain positive definiteness;
- We want our change to ensure that $B_{k+1}(\vx^{k+1}-\vx^k) = \nabla f(\vx^{k+1}) - \nabla f(\vx^{k})$.

The BFGS step update is:
$$ \begin{aligned}
B_{k+1} &= B_k + \alpha u_ku_k^T + \beta v_kv_k^T\\
u_k &= B_k(\vx^{k+1}-\vx^{k})\\
v_k & =  \nabla f(\vx^{k+1}) - \nabla f(\vx^{k})\\
\alpha &= -\frac{1}{(\vx^{k+1}-\vx^k)^TB_k^T(\nabla f(\vx^{k+1}) - \nabla f(\vx^{k}))}\\
\beta &=\frac{1}{(\nabla f(\vx^{k+1}) - \nabla f(\vx^{k}))^T(\nabla f(\vx^{k+1}) - \nabla f(\vx^{k})))}
\end{aligned} $$


Proposition (Sherman-Morrison formula [@Sherman]):  For any invertible square matrix $M$ and any vectors $u$ and $v$ such that $1+v^TM^{-1}u\neq 0$, 
$$
(M+uv^T)^{-1} = M^{-1} - \frac{M^{-1}uv^TM^{-1}}{1+v^TM^{-1}u}
$$

Proof:
$$\begin{aligned}
  (M+uv^T)\big(M^{-1} - \frac{M^{-1}uv^TM^{-1}}{1+v^TM^{-1}u}\big) &= MM^{-1} - M\frac{M^{-1}uv^TM^{-1}}{1+v^TM^{-1}u} + uv^TM^{-1} - uv^T\frac{M^{-1}uv^TM^{-1}}{1+v^TM^{-1}u}\\
  &= I - \frac{uv^TM^{-1}}{1+v^T M^{-1}u} + uv^TM^{-1} - uv^T\frac{M^{-1}uv^TM^{-1}}{1+v^TM^{-1}u}\\
  &= I + uv^TM^{-1}\big(1 - \frac{1}{1+v^T M^{-1}u} - \frac{uv^TM^{-1}}{1+v^TM^{-1}u}\big)\\
  &= I + uv^TM^{-1}\big(1 - \frac{1 + uv^TM^{-1}}{1+v^TM^{-1}u}\big)\\
  &= I \end{aligned}$$

Because we purposefully performed a small change on $B_k$ to obtain $B_{k+1}$, we can apply the Sherman-Morrison formula to obtain $B_{k+1}^{-1}$ from $B_k^{-1}$ directly. Let $y_k = x_{k+1}-x_k = \alpha_k d_k$.
\begin{equation}\label{eq:BFGS_Update}
B_{k+1}^{-1} = B_k^{-1} + \frac{\big(y_k^T v_k + v_k^TB_k^{-1}v_k\big)(y_ky_k^T)}{\big(y_k^Tv_k\big)^2} - \frac{B_k^{-1}v_ky_k^T + y_kv_k^TB_k^{-1}}{y_k^Tv_k}
\end{equation} 

The pseudocode for the BFGS method is thus:
\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwInOut{Input}{Input}
\SetKwInOut{Initialise}{Initialise}
\Input{a starting point \(x_0\), and initial approximation of the Hessian $M_0 = B_0^{-1}$.}
\Initialise{Set $k=0$.}
\While{$\nabla f(x_k)\neq 0$}{
Set $d_k=-M_k(\nabla f(x_k))$\;
Set $x_{k+1} = x_k + \alpha_k d_k$\;
Update $M_{k+1} = B_{k+1}^{-1} $ using \eqref{eq:BFGS_Update}.\;
Set $k = k+1$\;
}
\caption{BFGS Method \cite{Fletcher}}
\end{algorithm}

Note: Usually $B_0=I$.

# Constrained Optimisation

In some cases, we want our solution to satisfy some constraints - for instance we may want some or all of the coefficients to be nonnegative. In this case, both our optimality conditions and algorithms must take the set $\mathcal{X}$ into account.

Theorem (First-order optimality conditions for constrained problems):  Suppose that $f$ is differentiable at $\vx^*$. Then
if $f$ attains its minimum at $\vx^*$, then $$\langle \nabla f(\vx^*),\vx-\vx^*\rangle \geq 0, \forall \vx\in \mathcal{X}$$

A particularly important instance of constrained problems is when the constraints are given by means of inequalities (or equalities): 
$$
\mathcal{X} = \{\vx\in \R^n: g_i(\vx)\leq 0, \forall i=1,\ldots,m\}
$$

\begin{figure}
\begin{tikzpicture}
\fill[blue,opacity=0.2] (1,1) -- (2,4) -- (4,2);
\draw[thick] (0.667,0) -- (2.333,5) node[left] {$g_1$};
\draw[thick] (1,5) -- (5,1) node[left] {$g_2$};
\draw[thick] (0,0.667) -- (5,2.3333) node[above] {$g_3$};
\fill (3,3) circle (3pt);
\draw[->,very thick] (3,3) node[right] {$x_1$} --  (2.5,2.5);
\fill (4,2) node[below] {$x_2$}  circle (3pt);
\draw[->,very thick] (4,2) --  (3.5,1.5);
\draw[->,very thick] (4,2) --  (3.83333,2.5);
\fill (2,2) node[left] {$x_3$}  circle (3pt);
\end{tikzpicture}\caption{The arrows show "permissible" directions according to each constraint. Note that at Point $x_2$, a feasible direction must be at a below 90° angle with each arrow.}\label{fig:constrained}
\end{figure}

In [@fig:constrained], the arrows point in the directions where the points remain feasible. At the point $x_1$, anything that goes "towards" the black arrow (which is the gradient of the function $g_2$) will work. At the point $x_2$, the directions must be "towards" boths black arrows. From point $x_3$, every direction can lead to feasible solutions.

More formally, the directions must form an acute angle with the black arrows - in other words, their dot product must be positive. Note that what matters here is when the point lies on the boundary of the feasible set.

Definition (feasible direction):
Define the *active set* at the point $\vx$ as $A(\vx) = \{i=1,\ldots,m: g_i(\vx)=0\}$. A direction $\vd$ is a *feasible direction* from $\vx$ if $\langle \vx, \vd\rangle \leq 0$.

Theorem (First-order optimality conditions for constrained problems):  Suppose that $f$ is differentiable at $\vx^*$. Then
if $f$ attains its minimum at $\vx^*$, then $\langle \nabla f(\vx^*),\vd\rangle \geq 0$ or there exists $i\in A(\vx^*)$ such that $\langle \nabla g_i(\vx^*),\vd\rangle \geq 0$.

Proof
: Suppose that there is a direction which is both a descent direction (ie, $\langle \nabla f(\vx), \vd\rangle \leq 0$) and a feasible direction (ie, for any $i\in I$), $\langle \nabla g(\vx), \vd\rangle \leq 0$. Then there exists $\alpha_0>0$ such that for any $\alpha\in (0,\alpha_0)$, $f(\vx+\alpha \vd)<f(\vx)$ and $g_i(\vx+\alpha\vd)<g_i(\vx)$ for any  $i\in I$.

  Since for $i\notin I$, $g_i(\vx)<0$, that strict inequality remains true in a neighbourhood of $\vx$: there is $\alpha_1>0$ such that for any $\alpha\in (0,\alpha_1)$, $g_i(\vx+\alpha \vd)<0$.

  Taking $\alpha \in (0,\min(\alpha_0,\alpha_1))$, the solution $\vx+\alpha\vd$ is a better feasible solution than $\vx$.

### Conditional Gradient Method

Generating a feasible descent direction is not as straightforward as in the unconstrained case. When the constraints are simple enough (for example if they are linear), then, we can use the *conditional gradient method*. This method finds a direction that takes the feasible set into account. 

So, at every iteration we solve the problem:

$$ \text{minimise } \langle\nabla f(\vx^k),\vx-\vx^k\rangle, \text{ subject to } \vx\in \mathcal{X} $$

\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwInOut{Input}{Input}
\SetKwInOut{Initialise}{Initialise}
\Input{a starting point \(x_0\), and initial approximation of the Hessian $B_0$.}
\Initialise{Set $k=0$.}
\While{$\nabla f(x_k)\neq 0$}{
Let $d_k$ be the solution to:
$$
\text{minimise } \langle\nabla f(\vx^k),\vd\rangle, \text{ subject to } (\vx^k+\vd)\in \mathcal{X}
$$
Set $d_k=-B_k\nabla f(x_k)$\;
Set $x_{k+1} = x_k + \alpha_k d_k$\;
Update $B_k$ using \eqref{eq:BFGS_Update}.\;
Set $k = k+1$\;
}
\caption{Conditional Gradient  Method}
\end{algorithm}

### Penalty Methods

The conditional gradient method is only implementable when the constraints are simple enough. A more robust method, from that perspective, is the penalisation approach. The idea is to transform the original problem into an unconstrained problem, but to penalise solutions that are infeasible. A good penalty function should be 0 inside the feasible set, and positive outside. For numerical purpose, the more the constraints are violated, the higher one wants the penalty to be.

The new problem is:

$$ \text{minimise } f(\vx) + c P(\vx), \text{ subject to } \vx\in \R^n $$

The most common penalisation is:

- $\sum_{j=1}^m h_j^2(\vx)$ if $h_j=0$ are *equality constraints*;
- $\sum_{i=1}^m \max(0,g_i(\vx))^2$ if $g_i=0$ are *inequality constraints*;

Sometimes one may also chose a "sharp" penalisation:

- $\sum_{j=1}^m |h_j(\vx)|$ if $h_j=0$ are *equality constraints*;
- $\sum_{i=1}^m \max(0,g_i(\vx))$ if $g_i=0$ are *inequality constraints*;

The "sharp" penalisation is not differentiable, but on the other hand, the gradient of the penalty doesn't become very small when approaching the feasible set.
