-- amsthms.lua
-- This is a pandoc filter to create latex environments.

-- It converts divs with a specific class to the latex environment with the same name.
-- 
-- Example:
-- 
-- ::: theorem
-- Text of the theorem
-- :::
-- 
-- will be converted to:
-- \begin{theorem}
-- Text of the theorem
-- \end{theorem}
-- 
-- Alternatively, we can also have a theorem (or another environment) on a single paragraph, as follows:
-- 
-- Theorem: Text of the theorem...
-- 
-- will be converted to:
-- \begin{theorem}
-- Text of the theorem
-- \end{theorem}
-- 
-- 
-- 
-- The list of environment is stored in the variable amsthms. The default environments are "theorem","proposition","lemma","definition","proof", and "example"
-- 
-- To specify a different list, give it in the yaml header, as follows:
-- amsthms: [definiton, theorem, note]
--
-- or:
--
-- amsthms:
--   - definition
--   - theorem
--   - note

local thms = pandoc.List{"theorem","proposition","lemma","remark","definition","proof","example","exercise","solution","prompt","answer","question","feedback","corollary","note","notes"}
local nonewslide = pandoc.List{"answer","solution","hint"}
local exercises = pandoc.List{"exercise","prompt"}

--- Add a block to the document's header-includes meta-data field.
-- @param meta the document's metadata block
-- @param blocks list of Pandoc block elements (e.g. RawBlock or Para)
--    to be added to the header-includes of meta
-- @return meta the modified metadata block
local function add_header_includes(meta, blocks)

  -- add any exisiting meta['header-includes']
  -- it could be a MetaList or a single String
  if meta['header-includes'] then
    header_includes = meta['header-includes']
    if meta['header-includes'].t == 'MetaList' then
      header_includes:insert(pandoc.MetaBlocks({blocks}))
    else
      meta['header-includes'] = pandoc.MetaBlocks({header_includes,{blocks}})
    end
  else
      meta['header-includes'] = pandoc.MetaBlocks({blocks})
  end

  return meta
end

---- Generate the latex preamble commands to define relevant environments.
-- Reads the thms list specified in the metadata (if it exists), and
-- generate a list of "\newtheorem" commands to define these environments.
-- @param meta the document's metadata block
-- @return meta the modified metadata block
local function generateEnvironments(meta)
  if meta['thms'] then
    thms = pandoc.List(meta['thms'])
    thms = thms:map(pandoc.utils.stringify)
  end
  local headers = "\\usepackage{amsthm}\n"
  for _,thm in pairs(thms) do
    headers = headers .. "\\ifdefined\\" .. thm .. "\\else\\newtheorem{" .. thm .. "}{\\MakeUppercase " .. thm .. "}\\fi\n"
  end
  return add_header_includes(meta,pandoc.RawBlock("latex",headers))
end

---- Check whether a string ends with a colon.
-- @param x a pandoc object (block or inline)
-- @return true if the object is of type Str and the last character is ":"
function endsWithColon(x)
  if x.t ~= "Str" then return false end
  local word = x.text
  local n = word:len()
  return word:sub(n,n) == ":"
end

---- Check whether a string starts with a bracket.
-- @param x a pandoc object (block or inline)
-- @return true if the object is of type Str and the first character is "("
function startsWithBracket(x)
  if x.t ~= "Str" then return false end
  local word = x.text
  return word:sub(1,1) == "("
end

---- Take all the items in a list starting from a given index.
-- @param c the list to slice
-- @param idx the index from which to slice
-- @ return content the new list with the elements in c after index idx.
function sliceAfter(c,idx)
  local content = pandoc.List{}
  local i = idx
  while c[i] ~= nil do
    content:insert(c[i])
    i = i+1
  end
  return content
end

---- Take all the items in a list up to a given index.
-- @param c the list to slice
-- @param idx the index up to which to slice
-- @return content the new list with the elements in c up to index idx.
function sliceBefore(c,idx)
  local content = pandoc.List{}
  for i=1,idx do content:insert(c[i]) end
  return content
end


---- Replace one-liner environment paragraphs to definition lists
-- If the paragraph has the format "word [(title)]: paragraph",
-- replace is with a definition list, where "word [(title)]" is the term and
-- "paragraph" is the definition.
-- @param para a paragraph to process
-- @return the defintion list if the paragraph is being replaced, nil otherwise.
function oneLineEnvironments(para)
  local colon,idx = para.content:find_if(endsWithColon)
  if idx ~= nil and para.content[1].t == "Str" then --and (idx <=2 or para.content[3].text:sub(1,1) == "(") then
    local word = para.content[1].text:lower()
    if idx == 1 then word = word:sub(1,word:len()-1) end -- strip the colon out
    if thms:includes(word) and (idx <= 2 or (pandoc.utils.stringify(para.content[3]):sub(1,1) == "(")) then
      if para.content[idx+1].t == "Space" then para.content:remove(idx+1) end
      local content = sliceAfter(para.content,idx+1)
      local definition = sliceBefore(para.content,idx-1)
      colon = colon.text
      colon = colon:sub(1,colon:len()-1)
      definition:insert(pandoc.Str(colon))
      return pandoc.DefinitionList{{definition,pandoc.Para(content)}}
    end
  end
end

---- Replace theorem-like definition lists with div environments with the appropriate class.
-- @param type the environment type (theorem, lemma, definition, etc.)
-- @param content the content of the environment
-- @param title the title of the enviroment.
-- @return a list of pandoc blocks.
local function generateHTMLEnvironment(type,content,title)
  if title then
    local spantitle = pandoc.Span(title)
    spantitle.classes = {"title"}
    content:insert(1,pandoc.Plain(spantitle))
  end
  div = pandoc.Div(content)
  div.classes={type}
  return(div)
end


---- Generate a latex environment from a title and a
-- @param type the environment type (theorem, lemma, definition, etc.)
-- @param content the content of the environment
-- @param title the title of the enviroment.
-- @return a list of pandoc blocks
local function generateLaTeXEnvironment(type,content,title)
  local environment = "\\begin{" .. type .. "}"
  local open = pandoc.List{}
  local open = pandoc.List{pandoc.RawInline("latex",environment)}
  if title then
    open:insert(pandoc.RawInline("latex","["))
    if exercises:includes(type) then
      open:insert(pandoc.Str("subtitle="))
    end
    open:extend(title)
    open:insert(pandoc.RawInline("latex","]"))
  end
  div = pandoc.List{pandoc.Plain(open)}
  div:extend(content)
  div:insert(pandoc.RawBlock("latex","\\end{" .. type .. "}"))
  return div
end

---- Replace theorem-like definition lists with div environments with the appropriate class.
-- If the term defined is in the list of theorems thms, then replace this with a div
-- with the class given by the term, and the content by the definition. Optionally
-- we can specify a title, in brackets after the term. The title will be put in a span
-- with class "title".
-- @param dl a definition to process
-- @return a div that replaces dl if created, nil otherwise.
local function createEnv(dl)
  local blocks= pandoc.List()
  local newdl = pandoc.List()
  for _,val in pairs(dl.content) do
    local def = val[1]
    local content = val[2][1]
    local type = def[1].text:lower()
    if(endsWithColon(type)) then type = type:sub(1,type:len()-1) end
    if thms:includes(type) then
      if(newdl[1]) then
        blocks:insert(pandoc.DefinitionList(newdl))
        newdl = pandoc.List{}
      end
      w1,idx = def:find_if(startsWithBracket)
      local title=nil
      if(idx ~= nil) then
        w1 = w1.text:sub(2)
        title = pandoc.List{}
        if (w1) then title:insert(pandoc.Str(w1)) end
        title:extend(sliceAfter(def,idx+1))
        l1 = title:remove().text
        if(l1:len() > 1) then title:insert(pandoc.Str(l1:sub(1,l1:len()-1))) end
      end
      if FORMAT == "beamer" or FORMAT == "revealjs" then
        if not nonewslide:includes(type) then
          blocks:insert(pandoc.HorizontalRule())
        end
      end
      if FORMAT:sub(1,4) == "html" or FORMAT == "revealjs" then
        blocks:insert(generateHTMLEnvironment(type,content,title))
      elseif FORMAT == "latex" or FORMAT == "beamer" then
        blocks:extend(generateLaTeXEnvironment(type,content,title))
      end
    else
      newdl:insert(val)
    end
  end
  blocks:insert(pandoc.DefinitionList(newdl))
  return blocks
end

function removeUnnecessaryRules(blocks)
  local newblocks = pandoc.List{}
  local ins = 0
  for k,b in pairs(blocks) do
    if k == 1 or b.t ~= "HorizontalRule" or blocks[k-1].t ~= "Header" then
      ins = ins + 1
      newblocks:insert(b)
    else
      if blocks[k-1].level >= 2 then
        newblocks:insert(ins,b)
        ins = ins + 1
      end
    end
  end
  return newblocks
end


return {
{Meta = generateEnvironments},
{Para = oneLineEnvironments},
{DefinitionList = createEnv},
{Blocks = removeUnnecessaryRules}
}
